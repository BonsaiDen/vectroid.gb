# An Asteroids Game for Nintendo GameBoy

[ROM Download](https://gitlab.com/BonsaiDen/vectroid.gb/-/jobs/artifacts/master/raw/build/vectroid.gbc?job=build_rom)

A 2D vector based, action game for your favorite handheld gaming device.

Runs on both original DMG hardware as well as on GameBoy Color for improved graphical effects.

Developed using [gbc](https://gitlab.com/BonsaiDen/gbc-rs) and [gb-lib](https://gitlab.com/BonsaiDen/gb-lib).

Tested and debugged with [BGB](http://bgb.bircd.org/).


## Showcase

![Showcase Video](https://gitlab.com/BonsaiDen/vectroid.gb/raw/master/media/vectroid_beta3.webm)


## Copyright and License

Copyright (c) 2019 Ivo Wetzel. All rights reserved.

