; Full Color ------------------------------------------------------------------
GLOBAL PaletteBackground:
    ; Background Tiles
    GBC_COLOR(  2,   0,  16)
    GBC_COLOR( 64,  64,  64)
    GBC_COLOR(128, 128, 128)
    GBC_COLOR(255, 255, 255)

    ; Ship colors
GLOBAL PaletteShipLookup:
    GBC_COLOR(  0,   0,   0)
    GBC_COLOR(255,   0,   0)
    GBC_COLOR(255, 255,   0)
    GBC_COLOR(  0,   0, 255)

    ; TODO final color adjust for dust particles
    GBC_COLOR(  2,   0,  16)
    GBC_COLOR( 92,  67,  50)
    GBC_COLOR(146,  90,  51)
    GBC_COLOR(187, 120,  74)

GLOBAL _PaletteLogo:
    ; Logo 1 Leafs
    GBC_COLOR(  0,   0,  16)
    GBC_COLOR( 20, 170,  20)
    GBC_COLOR(135, 200,  55)
    GBC_COLOR(120, 220, 120)

    ; Logo 2 Name
    GBC_COLOR(  0,   0,  16)
    GBC_COLOR( 70,  60,  10)
    GBC_COLOR(255,   0,   0)
    GBC_COLOR(245, 225,  50)

    ; Logo Bark
    GBC_COLOR(  0,   0,  16)
    GBC_COLOR( 20, 170,  20)
    GBC_COLOR(150,  70,  40)
    GBC_COLOR(135, 200,  55)

    ; Logo Intermediate
    GBC_COLOR(  0,   0,  16)
    GBC_COLOR( 20, 170,  20)
    GBC_COLOR(150,  70,  40)
    GBC_COLOR(120, 220, 120)

GLOBAL PaletteSprite:

    ; Asteroids
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(187, 120, 74)
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(0, 0, 0)

    ; Asteroids 2
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(226, 232, 247)
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(0, 0, 0)

    ; Ship
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(255, 0, 0)
    GBC_COLOR(51, 152, 56); Shield color A
    GBC_COLOR(170, 230, 170); Shield color B

    ; Bullet / Bomb A
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(255, 255, 0)
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(0, 0, 0)

    ; Thrust A
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(255, 255, 0)
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(0, 0, 0)

    ; Thrust B / Bomb B
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(255, 64, 64)
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(0, 0, 0)

    ; Asteroids Title
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(128, 81, 52)
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(0, 0, 0)

    ; Boss
    GBC_COLOR(0, 0, 0)
    ; copied from PaletteBossLookup at runtime
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(0, 0, 0)

GLOBAL PaletteBossLookup:
    ; Boss colors - only one will be used per boss
    GBC_COLOR(255,   0,   0)
    GBC_COLOR(  0, 255,   0)
    GBC_COLOR(  0,   0, 255)
    GBC_COLOR(255,   0, 255)

GLOBAL PaletteBackgroundRetro:
    ; Background Tiles
    GBC_COLOR(  2,   6,  12)
    GBC_COLOR( 45, 106,  88)
    GBC_COLOR(135, 195, 117)
    GBC_COLOR(196, 245, 199)

    ; Ship colors
GLOBAL _PaletteShipLookupRetro:
    GBC_COLOR(255,   0,   0)
    GBC_COLOR(255, 255,   0)
    GBC_COLOR(  0, 255,   0)
    GBC_COLOR(  0,   0, 255)

    ; TODO final color adjust for dust particles
    GBC_COLOR(  2,   6,  12)
    GBC_COLOR( 45, 106,  88)
    GBC_COLOR(135, 195, 117)
    GBC_COLOR(196, 245, 199)

; Retro Color -----------------------------------------------------------------
GLOBAL PaletteSpriteRetro:

    ; Asteroids
    GBC_COLOR(0, 0, 0)
    GBC_COLOR( 94, 149,  99)
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(0, 0, 0)

    ; Asteroids 2
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(236, 251, 237)
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(0, 0, 0)

    ; Ship
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(158, 206, 141)
    GBC_COLOR(115, 180, 115); Shield color A
    GBC_COLOR(200, 220, 200); Shield color B

    ; Bullet / Bomb A
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(236, 251, 237)
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(0, 0, 0)

    ; Thrust A
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(236, 251, 237)
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(0, 0, 0)

    ; Thrust B / Bomb B
    GBC_COLOR(0, 0, 0)
    GBC_COLOR( 45, 106,  88)
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(0, 0, 0)

    ; Asteroids Title
    GBC_COLOR(0, 0, 0)
    GBC_COLOR( 45, 106,  88)
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(0, 0, 0)

    ; Boss
    GBC_COLOR(0, 0, 0)
    ; retro colors are fixed
    GBC_COLOR(236, 251, 237)
    GBC_COLOR(0, 0, 0)
    GBC_COLOR(0, 0, 0)

; Super GameBoy----------------------------------------------------------------
GLOBAL SGBGamePalette:
    ; #0: Game Color Palette
    SGB_COLOR(  0,   0,  16); due to how SGB works, this color is shared across all following entries
    SGB_COLOR(255,  64,  64)
    SGB_COLOR(187, 120,  74); asteroids
    SGB_COLOR(255, 255, 255)

    ; Tree Trunk and Logo Text
    SGB_COLOR(  0,   0,   0); shared from palete #{}
    SGB_COLOR( 20, 170,  20); green for leafs
    SGB_COLOR(150,  70,  40); tree bark
    SGB_COLOR(245, 225,  50); logo yellow

    ; Tree Top (Greens)
    SGB_COLOR(  0,   0,   0); shared from palete #{}
    SGB_COLOR( 20, 170,  20)
    SGB_COLOR(135, 200,  55)
    SGB_COLOR(120, 220, 120)

    ; Unused at the moment
    SGB_COLOR(  0,   0,   0); shared from palete #{}
    SGB_COLOR(  0,   0,  96)
    SGB_COLOR(  0,   0, 160)
    SGB_COLOR(  0,   0, 255)

;MACRO GBC_COLOR_HEX(@hex)
;    DB (FLOOR(((@hex >> 8) & $FF) / 8) & 31); Green
;    DB (FLOOR(((@hex & $FF) / 8) & 31); Blue
;    DB (FLOOR((@hex >> 16) / 8) & 31); Red
;ENDMACRO

MACRO GBC_COLOR(@r, @g, @b)
    DB (FLOOR(@g / 8) & 31)
    DB (FLOOR(@b / 8) & 31)
    DB (FLOOR(@r / 8) & 31)
ENDMACRO

