; Constant --------------------------------------------------------------------
CONST LINE_MASK_LOCATION    $FF00


; RAM -------------------------------------------------------------------------
SECTION "LineRenderJumpTable",WRAM0[$C400]
lineRenderJumpTable:        DS 16

SECTION "LineMask",HRAM
lineMasks:                  DS 8


; Line Plotting ---------------------------------------------------------------
SECTION "LineRender",ROM0
GLOBAL line_init:
    ; setup pixel column / line mask in HRAM
    ld      bc,(8 << 8) | (lineMasks - LINE_MASK_LOCATION)
    ld      a,%1000_0000
.next_column:
    ld      [c],a
    srl     a
    inc     c
    djnz    .next_column

    ; copy the jump table layout into memory at $CX00
    ld      de,lineRenderJumpTable
    ld      hl,line_render_jump_table
    ld      b,8
.next_pointer:
    ldxa    [de],[hli]
    inc     e
    ldxa    [de],[hli]
    inc     e
    djnz    .next_pointer
    ret

GLOBAL line_two:
    ; calculate jump table index (0-3) for unrolled line sector routine into b register
    ;   0 = -1, -1
    ;   4 = -1,  1
    ;   8 =  1, -1
    ;  16 =  1,  1
    DELTA_X(d, b, h, 0, 8, b)
    DELTA_Y(e, c, l, 0, 4, b)

    ; skip lines with length 0
    ld      a,l
    add     h
    ret     z

    ; check if dy <= dx
    ld      a,l
    cp      h
    jr      nc,.horizontal_line

    ; offset jump table entry for matching octants
.vertical_line:
    inc     b; add 2 bytes
    inc     b

.horizontal_line:
    ; store dx/dy for line drawing
    push    hl

    ; load function pointer for line drawing
    ld      h,lineRenderJumpTable >> 8
    ld      l,b

    ; compute initial line mask value
    ; will later be updated every time the X pixel coordinate changes
    ; during line drawing
    ld      a,d
    and     %0000_0111
    add     lineMasks - LINE_MASK_LOCATION
    ld      c,a
    ld      a,[c]
    ld      b,a; prepare initial line mask value in b

    ; setup jump pointer to line octant routine
    ld      a,[hli]
    ld      h,[hl]
    ld      l,a

    ; adjust error offset value for unsigned computation in line drawing
    ld      a,128

    ; jump to octant routine
    jp      hl

line_render_jump_table:
    DW      plot_line_dec_y_dec_x; y
    DW      plot_line_dec_x_dec_y; x
    DW      plot_line_inc_y_dec_x; y
    DW      plot_line_dec_x_inc_y; x
    DW      plot_line_dec_y_inc_x; y
    DW      plot_line_inc_x_dec_y; x
    DW      plot_line_inc_y_inc_x; y
    DW      plot_line_inc_x_inc_y; x

plot_line_dec_y_dec_x:
    PLOT_LINE(e, d, l, h, `dec @first`, `dec @second`, ``, `rlc b`)

plot_line_dec_x_dec_y:
    PLOT_LINE(d, e, h, l, `dec @first`, `dec @second`, `rlc b`, ``)

plot_line_inc_y_dec_x:
    PLOT_LINE(e, d, l, h, `inc @first`, `dec @second`, ``, `rlc b`)

plot_line_dec_x_inc_y:
    PLOT_LINE(d, e, h, l, `dec @first`, `inc @second`, `rlc b`, ``)

plot_line_dec_y_inc_x:
    PLOT_LINE(e, d, l, h, `dec @first`, `inc @second`, ``, `rrc b`)

plot_line_inc_x_dec_y:
    PLOT_LINE(d, e, h, l, `inc @first`, `dec @second`, `rrc b`, ``)

plot_line_inc_y_inc_x:
    PLOT_LINE(e, d, l, h, `inc @first`, `inc @second`, ``, `rrc b`)

plot_line_inc_x_inc_y:
    PLOT_LINE(d, e, h, l, `inc @first`, `inc @second`, `rrc b`, ``)


; Macros ----------------------------------------------------------------------
; -----------------------------------------------------------------------------
MACRO DELTA_X(@start, @end, @delta, @one, @two, @tableIndex)
delta:
    ; dx = ex - sx
    ld      a,@end
    sub     @start
    jr      nc,.positive; dx < 0

    ; dx = -dx
    neg
    ld      @tableIndex,@one
    jr      .negative

.positive:
    ld      @tableIndex,@two
.negative:
    ld      @delta,a; dx
ENDMACRO

MACRO DELTA_Y(@start, @end, @delta, @one, @two, @tableIndex)
delta:
    ; dy = ey - sy
    ld      a,@end
    sub     @start
    jr      nc,.positive; dy < 0

    ; dy = -dy
    neg
    ld      @delta,a; dy
    jr      .negative

.positive:
    ld      @delta,a; dy
    ld      a,@tableIndex
    add     @two
    ld      @tableIndex,a
.negative:
ENDMACRO

MACRO PLOT_LINE(@first, @second, @delta_one, @delta_two, @update_one, @update_two, @mask_one, @mask_two)
plot_line:
    ; restore deltas
    pop     hl

    ; setup remaining line length for first coordinate
    ld      c,@delta_one
    inc     c; draw one additional pixel so we actually reach the target point
             ; this causes minimal overdraw when joining multiple lines but
             ; also looks more consistent

    ; offset first delta for slope error comparison
    set     7,@delta_one

    ; adjust second delta for slope calculation
    sla     @delta_two

.loop:
    ; store initial / updated slope error
    ld      [lineError],a

    PLOT_PIXEL()

    ; error += d2
    ld      a,[lineError]
    add     @delta_two
    cp      @delta_one

    ; jump if error < delta_one
    jr      c,.no_second_increase

    ; error -= d1 * 2
    sub     @delta_one
    sub     @delta_one

    ; update second coordinate
    @update_two

    ; update pixel mask for second coordinate
    @mask_two

.no_second_increase:
    ; update first coordinate
    @update_one

    ; update pixel mask for first coordinate
    @mask_one

    ; decrease remaining line length for first coordinate
    dec     c
    jr      nz,.loop
    ret
ENDMACRO

MACRO PLOT_PIXEL()
    push    hl

    ; calculate tile grid index (px / 8 + y / 8 * 4) of the sprite
    ; then lookup the actual VRAM location from the sprite's assigned lookup table

    ; |--------|--------|
    ; |        |        |
    ; |   1    |   2    |
    ; |        |        |
    ; |--------|--------|
    ; |        |        |
    ; |   3    |   4    |
    ; |        |      p |
    ; |--------|--------|

    ; first compute Y tile grid index (py / 8 * 4)
    ld      a,e
    rra
    and     %0001_1100
    ld      l,a; store y index

    ; then compute X tile grid index (px / 8)
    ld      a,d
    rla
    swap    a
    and     %0000_0111

    ; and combine with y
    add     l

    ; now multiply by 2 for 16 bit offset address indexing
    add     a

    ; load polygon tile address table base offset
    ld      hl,polygonOffset + 1

    ; combine base low byte with tile grid index from above
    add     [hl]

    ; move to high byte and combine
    dec     l
    ld      h,[hl]
    ld      l,a

    ; HL now points into the sprite's tile pointer table for the correct tile
    ; hl-------|
    ; |        |
    ; |   4    |
    ; |      p |
    ; |--------|

    ; we now compute the line offset for the tile
    ld      a,e
    and     %0000_0111
    add     a; x2 - since we only use 2 colors we can skip the "high bits" of each line

    ; and combine it with the base pointer for the tile
    add     [hl]
    inc     l

    ; next we load the high byte from the table and assemble HL into the final
    ; line address pointer
    ld      h,[hl]
    ld      l,a

    ; now we load the current pixel data from the offscreen buffer
    ; hl |101010p0|
    ld      a,[hl]

    ; and combine it with the pixel mask for the current column
    ; hl |10101010|
    or      b

    ; then store the combined line data back into the offscreen buffer
    ld      [hl],a

    pop     hl
ENDMACRO

