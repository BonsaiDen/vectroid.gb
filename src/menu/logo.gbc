; Logo Menu Logic -------------------------------------------------------------
SECTION "MenuLogo",ROM0
GLOBAL menu_game_logo_init:
    ; reset state
    call    game_reset_systems_and_sfx

    ; skip color palette setup in DMG mode
    ld      a,[coreColorEnabled]
    cp      0
    jr      z,.dmg

    ; override palette mode for logo
    ld      a,[paletteMode]
    and     %0000_0001
    ld      [paletteMode],a
    call    palette_set_logo_bg_count

    ; logo color map
    call    lz4_decode(palette_bg_color_map_logo, polygonOffscreenBuffer)
    ld      hl,$9886
    ld      bc,$080A
    ld      de,polygonOffscreenBuffer
    call    palette_load_bg_color_map

    ; play logo animation
.dmg:
    ld      a,[paletteSGB]
    cp      0
    jr      z,.no_sgb

    ; set sgb logo palettes
    ld      hl,SGBLogoAttr
    call    sgb_packet_transfer

.no_sgb:
    ld      hl,LogoData
    ld      de,polygonOffscreenBuffer
    ld      bc,$0604
    ld      a,12
    call    animation_play

    sfx(mmp_track_sfx_logo_x, SOUND_PRIORITY_HIGH)

    ldxa    [logoDelay],160
    ret

GLOBAL menu_game_logo_update:
    ; skip if we are already transitioning to title
    ld      a,[logoDelay]
    cp      0
    ret     z

    ; skip logo via start button
    ld      a,[coreInputOn]
    cp      BUTTON_START
    jr      z,.skip

    ; playback logo animation
    call    animation_update

    ; reduce delay until we switch to title
    decx    [logoDelay]
    ret     nz

.skip:
    call    mmp_reset
    xor     a
    ld      [logoDelay],a
    switchGameMode(GAME_MODE_TITLE)
    ret

SGBLogoAttr:
    DB ($04 << 3) + 3
    DB 6; # of data sets
    SGB_SET_COLOR_INISDE(1, 8, $8, $B, $B); trunk main
    SGB_SET_COLOR_INISDE(2, 6, 4, $8, $9); leafs top left
    SGB_SET_COLOR_INISDE(2, $B, 9, $D, $B); leafs bottom right
    SGB_SET_COLOR_INISDE(2, 9, 4, $D, $7); leafs top left
    SGB_SET_COLOR_INISDE(2, $A, $6, $E, $8); leafs middle right
    SGB_SET_COLOR_INISDE(1, $6, $C, $D, $E); title text

MACRO SGB_SET_COLOR_INISDE(@palette, @left, @top, @right, @bottom)
    DB %0000_0001; inside
    DB @palette | @palette << 2
    DB @left
    DB @top
    DB @right
    DB @bottom
ENDMACRO

